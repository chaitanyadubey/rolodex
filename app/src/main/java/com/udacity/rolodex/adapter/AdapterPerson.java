package com.udacity.rolodex.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.squareup.picasso.Picasso;
import com.udacity.rolodex.R;
import com.udacity.rolodex.entity.Person;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Created by DELL on 9/15/2017.
 */

public class AdapterPerson extends RecyclerView.Adapter<AdapterPerson.ViewHolder> {


    private ArrayList<Person> personArrayList;


    public void clearList()
    {
        personArrayList.clear();
    }

    public ArrayList<Person> getPersonArrayList() {
        return personArrayList;
    }

    public void setPersonArrayList(ArrayList<Person> personArrayList) {
        this.personArrayList = personArrayList;
    }

    private Context context;

    public AdapterPerson(Context context, ArrayList<Person> personArrayList) {
        this.context = context;
        this.personArrayList = personArrayList;

    }

    @Override
    public AdapterPerson.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_person_details, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {

        Person person = personArrayList.get(i);

        Picasso.with(context).load(person.getAvatar()).resize(300, 300).into(viewHolder.avatar_ImageView);

        viewHolder.first_name_textView.setText(person.getFirstName());

        viewHolder.last_name_textView.setText(person.getLastName());

        viewHolder.email_textView.setText(person.getEmail());

        viewHolder.company_textView.setText(person.getCompany());

        viewHolder.startDate_textView.setText(person.getStartDate());

        String formattedBio = person.getBio();

        formattedBio = formatToHTML(formattedBio,"\\*","<b>","</b>");

        formattedBio = formatToHTML(formattedBio,"_","<i>","</i>");

        Spanned myStringSpanned = Html.fromHtml(formattedBio, null, null);
        viewHolder.bio_textView.setText(myStringSpanned, TextView.BufferType.SPANNABLE);
    }


    public String formatToHTML(String str,String symbolTag, String htmlTagStart,String htmlTagEnd)
    {
       StringBuffer replaceString = new StringBuffer();
       StringTokenizer st2 = new StringTokenizer(str, symbolTag);

        int count = 1;

        while (st2.hasMoreElements())
        {
            replaceString.append(st2.nextToken());

            if(st2.hasMoreElements())
            {
                if(count%2==1)
                {
                    replaceString.append(htmlTagStart);
                }
                else
                {
                    replaceString.append(htmlTagEnd);
                }
            }
            count++;
        }
        return replaceString.toString();

    }
    @Override
    public int getItemCount() {
        return personArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView first_name_textView;

        TextView last_name_textView;

        TextView email_textView;

        TextView company_textView;

        TextView startDate_textView;

        TextView bio_textView;

        ImageView avatar_ImageView;


        public ViewHolder(View view)
        {
            super(view);

            avatar_ImageView = (ImageView)view.findViewById(R.id.avatar_imageView);

            first_name_textView = (TextView)view.findViewById(R.id.first_name_textView);

            last_name_textView = (TextView)view.findViewById(R.id.last_name_textView);

            email_textView = (TextView)view.findViewById(R.id.email_textView);

            company_textView = (TextView)view.findViewById(R.id.company_textView);

            startDate_textView = (TextView)view.findViewById(R.id.startDate_textView);

            bio_textView = (TextView)view.findViewById(R.id.bio_textView);



        }
    }
}