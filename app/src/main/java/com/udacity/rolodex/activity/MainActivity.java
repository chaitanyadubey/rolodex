package com.udacity.rolodex.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.udacity.rolodex.R;
import com.udacity.rolodex.adapter.AdapterPerson;
import com.udacity.rolodex.entity.Person;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {


    public String TAG = "Udacity:"+getClass();


    private static final int mINTERNET_PERMISSON = 1;



    RecyclerView recyclerView;

    ProgressBar loading_progressBar;

    String jsonDataURL = "https://s3-us-west-2.amazonaws.com/udacity-mobile-interview/CardData.json";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_main);
        recyclerView = (RecyclerView)findViewById(R.id.card_recycler_view);
        recyclerView.setHasFixedSize(true);


        if(!hasAllPermissions())
        {
            Toast.makeText(this, "Does not have required permissions.", Toast.LENGTH_SHORT).show();
            return;
        }


        int numberOfColumns = 1;

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(),numberOfColumns,GridLayoutManager.HORIZONTAL  ,false);
        recyclerView.setLayoutManager(layoutManager);

        //ArrayList personArrayList = prepareData();

        ArrayList personArrayList = new ArrayList();

        AdapterPerson adapter = new AdapterPerson(getApplicationContext(),personArrayList);

        recyclerView.setAdapter(adapter);

        LoadData loadData = new LoadData();
        loadData.execute(jsonDataURL);


        loading_progressBar = (ProgressBar)findViewById(R.id.loading_progressBar);
        loading_progressBar.setVisibility(View.VISIBLE);




    }

    public boolean hasAllPermissions()
    {
        boolean hasAllPermissions = true;


        int permissionCheck;

        permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            hasAllPermissions = false;
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, mINTERNET_PERMISSON);
        }
        Log.d(TAG,"hasAllPermissions="+hasAllPermissions);
        return hasAllPermissions;
    }


////Test DATA
//    private ArrayList prepareData(){
//
//        ArrayList personArrayList = new ArrayList<>();
//
//        Person person = new Person();
//        person.setFirstName("Chaitanya");
//        person.setLastName("Dubey");
//        person.setEmail("chaitanyadubey@gmail.com");
//        person.setBio("I am an Android Programmer");
//        person.setAvatar("https://robohash.org/lacinia%40variusultricesmauris.net?bgset=bg2&set=set3");
//
//        personArrayList.add(person);
//
//
//        person = new Person();
//        person.setFirstName("Geetika");
//        person.setLastName("Dubey");
//        person.setEmail("geetikadubey@gmail.com");
//        person.setBio("She is my wife");
//        person.setAvatar("https://robohash.org/lacinia%40variusultricesmauris.net?bgset=bg2&set=set3");
//
//        personArrayList.add(person);
//
//        return personArrayList;
//    }



    class LoadData extends AsyncTask<String, Void, String> {
        String TAG = "Udacity"+getClass();


        @Override
        protected String doInBackground(String... params) {

            String jsonString = null;


            try {
                String urlString = params[0];
                HttpURLConnection urlConnection = null;
                URL url = new URL(urlString);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.setReadTimeout(10000 /* milliseconds */);
                urlConnection.setConnectTimeout(15000 /* milliseconds */);
                urlConnection.setDoOutput(true);
                urlConnection.connect();

                BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
                StringBuilder sb = new StringBuilder();

                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();

                jsonString = sb.toString();
                System.out.println("JSON: " + jsonString);
            } catch (IOException e) {

                Log.d(TAG, "Exception e=" + e.getMessage());
                e.printStackTrace();
            }

            return jsonString;
        }

        @Override
        protected void onPostExecute(String jsonString) {


            AdapterPerson adapter = (AdapterPerson)recyclerView.getAdapter();

            adapter.clearList();

            ArrayList<Person> personArrayList = adapter.getPersonArrayList();


            try {
                JSONArray jsonArray = new JSONArray(jsonString);

                for (int i = 0, size = jsonArray.length(); i < size; i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String firstName = jsonObject.getString("firstName");
                    String lastName = jsonObject.getString("lastName");
                    String email = jsonObject.getString("email");
                    String company = jsonObject.getString("company");
                    String startDate = jsonObject.getString("startDate");
                    String bio = jsonObject.getString("bio");
                    String avatar = jsonObject.getString("avatar");

                    Person person = new Person();

                    person.setFirstName(firstName);
                    person.setLastName(lastName);
                    person.setEmail(email);
                    person.setCompany(company);
                    person.setStartDate(startDate);
                    person.setBio(bio);
                    person.setAvatar(avatar);


                    personArrayList.add(person);


                }


                adapter.notifyDataSetChanged();
                recyclerView.setAdapter(adapter);



            } catch (JSONException e) {

                Log.d(TAG, "JSONException=" + e.getMessage());

                e.printStackTrace();
            }

            loading_progressBar.setVisibility(View.GONE);
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }

    }


}
